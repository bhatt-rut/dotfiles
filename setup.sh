#!/bin/bash

ln -s `pwd`/.gitconfig /home/$USER/.gitconfig
ln -s `pwd`/.tmux.conf /home/$USER/.tmux.conf
ln -s `pwd`/.vimrc /home/$USER/.vimrc
